# $1 - ip address

# Initialize swarm cluster
docker swarm init --advertise-addr $1

# Export worker join token to the file
docker swarm join-token --quiet worker > /vagrant/tokens/worker